#pragma once
#include<stdio.h>
#include<iostream>
#include<time.h>
using namespace std;
class Matrix
{
private:
	int rows;// ������
	int columns;// �������
	double **matrix;
	int check;
public:
	Matrix();
	Matrix(int N,int M) {
		rows = M;
		columns = N;
		matrix = new double*[rows];
		for (int i = 0; i < rows; i++)
			matrix[i] = new double[columns];
	}

	explicit Matrix(string titlefile);
	Matrix(const Matrix &obj);
	void matrixEnter();
	void matrixRand();
	void matrixShow();
	Matrix& operator=(const Matrix& m);
	Matrix& operator-=(const Matrix& m);
	Matrix& operator+=(const Matrix& m);
	Matrix& operator/=(const Matrix& m);
	Matrix& operator*=(const Matrix& m);

	Matrix  operator+(const Matrix& m);
	Matrix  operator-(const Matrix& m);
	Matrix  operator*(const Matrix& m);

	Matrix  operator/(const double k);
	Matrix  operator*(const double k);
	Matrix  operator+(const double k);
	Matrix  operator-(const double k);

	bool operator==(const Matrix& m) const;
	bool operator!=(const Matrix& m) const;

	double*& operator[](int index);
	//const double*& operator[](int index) const;

	void trans();
	double deter(int size,int pos, const Matrix& m);
	void newsizeMatrix(int newRows,int newColumns);
	void saveInFileMatrix(string titleFile);

	double determinantMatrix();
	Matrix reverseMatrix(Matrix& m);

	int getRows() {
		return rows;
	}
	int getColumns() {
		return columns;
	}
	~Matrix();
};

