#include "Matrix.h"
#include<stdio.h>
#include<iostream>
#include <fstream>
using namespace std;

Matrix::Matrix()
{
}


Matrix::Matrix(string titlefile)
{
	ifstream M(titlefile);
	if (!M.is_open()) {
		cout << "Faile to open file";
	}
	else {
		M >> columns >> rows;
		matrix = new double*[rows];
		for (int i = 0; i < rows; i++)
			matrix[i] = new double[columns];

		for (int i = 0; i <rows; i++) {
			for (int j = 0; j < columns; j++) {

				M>>matrix[i][j];
			}
		}
	}
}

Matrix::Matrix(const Matrix & obj)
{
	rows = obj.rows;
	columns = obj.columns;
	matrix = new double*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new double[columns];
	for (int i = 0; i <rows; i++) {
		for (int j = 0; j < columns; j++) {

			 matrix[i][j]=obj.matrix[i][j];
		}
	}

}

void Matrix::matrixEnter()
{
	cout << "Pleas, enter means for Matrix" << columns << "X" <<rows<<endl;

	for (int i = 0; i <rows ; i++) {
		for (int j = 0; j < columns; j++) {

			cin >>matrix[i][j];
		}
	}
	
}

void Matrix::matrixRand()
{
	srand(time(NULL));
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			 matrix[i][j]=(double)(rand()%10+1);
		}
	}
}

void Matrix::matrixShow()
{
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			cout << matrix[i][j] << "  ";

		}
		if(i!=rows-1)cout << endl;
	}
}

Matrix& Matrix::operator=(const Matrix& m)
{
	if (this !=&m) {
		rows = m.rows;
		columns = m.columns;

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				matrix[i][j] = m.matrix[i][j];
			}
		}
		return *this;
	}
}

Matrix & Matrix::operator-=(const Matrix & m)
{
	if (rows == m.rows && columns == m.columns) {


		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				matrix[i][j] = matrix[i][j] - m.matrix[i][j];
			}
		}
		return *this;
	}
	else cout << "Error, orders are not simple";


}

Matrix & Matrix::operator+=(const Matrix & m)
{
	if (rows == m.rows && columns == m.columns) {


		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				matrix[i][j] = matrix[i][j] + m.matrix[i][j];
			}
		}
		return *this;
	}
	else cout << "Error, orders are not simple";
}

Matrix & Matrix::operator/=(const Matrix & m)
{
	Matrix tmp(columns, rows);
	// TODO: insert return statement here
	return tmp;
}

Matrix & Matrix::operator*=(const Matrix & m)
{
	if (columns != m.rows) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < m.columns; j++) {
				double s = 0;
				for (int k = 0; k < columns; columns++)
					s = s + matrix[i][k] * m.matrix[k][j];
				
				matrix[i][j] = s;
			}
		}
		return *this;
	}
	else cout << "Error, different order";
}

Matrix Matrix::operator+(const Matrix & m)
{

	if (rows == m.rows && columns == m.columns) {
		Matrix tmp(rows, columns);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				tmp.matrix[i][j] = matrix[i][j]+ m.matrix[i][j];
			}
		}
		return tmp;
	}
	else cout << "Error, different order"<<rows<<" "<<m.rows<<" "<<columns<<" "<<m.columns;
}

Matrix Matrix::operator-(const Matrix & m)
{
	if (rows == m.rows && columns == m.columns) {
		Matrix tmp(rows,columns);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				tmp.matrix[i][j] = matrix[i][j] - m.matrix[i][j];
			}
		}
		return tmp;
	}
	else cout << "Error, different order";
}

Matrix Matrix::operator*(const Matrix & m)
{
	if (columns == m.rows) {
		double s = 0;
		Matrix tmp(rows, m.columns);
		for (int i = 0; i < rows; i++) {
			//cout << " 1 ";// << s;
			for (int j = 0; j < m.columns; j++) {
				//cout << " 2 ";// << s;

				for (int k = 0; k < columns; k++) {
					//cout << " 3 " << s;
					s = s + matrix[i][k] * m.matrix[k][j];
				}

				//cout << " " << s;
				tmp.matrix[i][j] =s;
			}
		}
		return tmp;
	}
	else cout << "Error, different order";
}

Matrix Matrix::operator/(const double k)
{
	if (k!=0.) {
		Matrix tmp(columns, rows);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j <columns; j++) {

				tmp.matrix[i][j] = matrix[i][j]/k;
			}
		}
		return tmp;
	}
	else cout << "Error, 0 was get";
}

Matrix Matrix::operator*(const double k)
{
	Matrix tmp(columns,rows);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {

			tmp.matrix[i][j] = k*matrix[i][j];
		}
	}
	return tmp;
}

Matrix Matrix::operator+(const double k)
{
	Matrix tmp(columns, rows);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {

			tmp.matrix[i][j] = matrix[i][j] + k;
		}
	}
	return tmp;
}

Matrix Matrix::operator-(const double k)
{
    Matrix tmp(columns, rows);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {

			tmp.matrix[i][j] = matrix[i][j]-k;
		}
	}
	return tmp;
}

bool Matrix::operator==(const Matrix & m) const
{
	int flag = 0;
	if(rows!=m.rows || columns!=m.columns)return false;
	else {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				if (matrix[i][j] != m.matrix[i][j]) flag = 1;
			}
		}
	}
	if (flag == 1)return false;
	else return true;
}

bool Matrix::operator!=(const Matrix & m) const
{
	int flag = 0;
	if (rows != m.rows || columns != m.columns)return true;
	else {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				if (matrix[i][j] == m.matrix[i][j]) { 
					flag = 1;
					break;
				}
			}
		}
	}
	if (flag == 1)return false;
	else return true;
}


double *& Matrix::operator[](int index)
{
	if ((index > 0 && index < rows) || (index > 0 && index < columns))
		return matrix[index];
}

//const double *& Matrix::operator[](int index) const
//{
	//if ((index > 0 && index < rows) || (index > 0 && index < columns))
	//	return matrix[index];
//	return ;
//}

void Matrix::trans()
{
	Matrix tmp(rows, columns);
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {

			tmp.matrix[j][i]= matrix[i][j];
		}
	}

	newsizeMatrix(columns,rows);
	if (rows != tmp.rows || columns != tmp.columns)
		cout << "atatata";
	for (int i = 0; i <rows;   i++) {
		for (int j = 0;  j < columns; j++) {

			matrix[i][j] = tmp.matrix[i][j];
		}
	}
}




void Matrix::newsizeMatrix(int newRows, int newColumns)
{
	for (int i = 0; i < rows; i++)
		delete matrix[i];
	delete matrix;

	rows = newRows;
	columns = newColumns;
	matrix = new double*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new double[columns];

}

void Matrix::saveInFileMatrix(string titleFile)
{
	ofstream save(titleFile);
	if (!save.is_open()) { cout << "faile to open file"; }
	else
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			save << matrix[i][j] << "  ";
		}
		if (i != rows - 1)save<< endl;
	}
}

double Matrix::determinantMatrix()
{
	if (rows != columns) {
		cout << "Cant found determinant";
	}
	else { 
		Matrix tmp(columns, columns);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				tmp.matrix[i][j] = matrix[i][j];
			}
		}
			return deter(columns, 0,tmp ); }
	return 0.0;
}

double Matrix::deter(int size, int pos, const Matrix& m) {
	double det = 0;
	double minor = 0;
	int kk = 0;
	int ii = 0;
	//if (size > 1) {
		for (int i = 0; i < size; i++) {
			ii = 0;
			kk = 0;
			Matrix Min(size - 1, size - 1);
			if (size > 0) {
				for (int k = 0; k < size - 1; k++) {
					ii = 0;
					for (int j = 0; j < size - 1; j++) {
						if (k == pos) { kk = 1; }
						if (i == j)ii = 1;
						Min.matrix[k][j] = m.matrix[k + kk][j + ii];

					}
				}
			}


			if (size - 1 > 0)minor = deter(size - 1, pos, Min);
			else {
				minor = m.matrix[0][0];
				return minor;

			}

			det = det + pow((-1), i)*m.matrix[pos][i] * minor;

		}
		return det;

}
Matrix Matrix::reverseMatrix(Matrix& m)
{
	Matrix tmp(rows,rows);
	double A = m.determinantMatrix();
	if (A == 0.) cout << "No revers Matrix";
	else {
		int ii = 0;
		int jj = 0;
		for (int i1 = 0; i1 < rows; i1++) {
			for (int j1 = 0; j1 < rows; j1++) {

				int ii = 0;
				int jj = 0;
				Matrix dop(rows - 1, rows - 1);
				for (int i = 0; i < m.rows - 1; i++) {
					jj = 0;
					for (int j = 0; j < m.rows - 1; j++) {
						if (i1 == i)ii = 1;
						if (j1 == j)jj = 1;
						dop.matrix[i][j] = m.matrix[i+ii][j+jj];
					}
				}

				tmp.matrix[i1][j1] = dop.determinantMatrix();
			}
		}
		tmp.trans();

		for (int i1 = 0; i1 < rows; i1++) {
			for (int j1 = 0; j1 < rows; j1++) {
				tmp.matrix[i1][j1] = tmp.matrix[i1][j1] / A;
			}
		}

	}
	return tmp;
}


Matrix::~Matrix()
{
	for (int i = 0; i < rows; i++)
		delete matrix[i];
	delete matrix;

}
